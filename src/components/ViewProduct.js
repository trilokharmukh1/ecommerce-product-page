import React from 'react';
import axios from 'axios';
import './../style/ViewProduct.css';
import Loader from './Loader';
import {Link} from 'react-router-dom'

class ViewProduct extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loaderFlag: true,
            product: "",
        }
    }

    componentDidMount() {
        axios({
            url: `products/${this.props.match.params.id.split(":")[1]}`,
            baseURL: "https://fakestoreapi.com",
            method: "get",
        })
            .then(response => {
                console.log("res");
                this.setState({ product: response.data });
            }).catch(error => {
                console.error('There was an error!', error);
            }).finally(() => {
                console.log("finally");
                this.setState({ loaderFlag: false })
            })
    }

    render() {
        return (
            <div className="view-product">
                {
                    this.state.loaderFlag &&
                    <Loader />
                }

                {
                    !this.state.loaderFlag &&
                    <div className='view-product-container'>

                        <div className="single-product-container" style={{ width: "600px" }}>
                            <div className="single-product" s>
                                <img className="single-image" src={this.state.product.image} alt="product" /><br />
                                <div className="product-details">
                                    <h3 >{this.state.product.title}</h3><br />
                                    <p className="view-product-description" > {this.state.product.description}</p><br />
                                    <p>Price: <span>${this.state.product.price}</span></p>
                                    <div>
                                        Rating: {this.state.product.rating["rate"]} ({this.state.product.rating.count})
                                    </div><br />
                                    <button className='single-button' onClick={()=>{this.props.addToCart(this.state.product)}}> Add to cart </button> <br />
                                    <Link to="/">
                                        <button className='single-button'> Back </button>
                                    </Link>
                                </div>
                            </div>
                        </div>

                    </div>

                }
            </div>
        );
    }
}

export default ViewProduct;