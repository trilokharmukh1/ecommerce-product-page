import './../style/Update.css'
import React from 'react';

class Update extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            titleError: false,
            descriptionError: false,
            priceError: false,
            popUp: false,
            updateField: true,
        }
    }

    popUp = (() => {
        this.setState({ popUp: true });
    });

    confirm = () => {
        this.setState({ popUp: false },
            (() => {
                this.setState({ updateField: true },
                    (() => {
                        this.props.updateDetails();
                    })
                )
            })
        )
    }

    cancel = () => {
        this.setState({ popUp: false },
            (() => {
                this.setState({ updateField: true })
            }))
    }

    validation = (event) => {
        event.preventDefault(); 

        this.setState({
            titleError: this.props.productDetails.title === "",
            descriptionError: this.props.productDetails.description === "",
            priceError: this.props.productDetails.price === "" || this.props.productDetails.price <= 0 || isNaN(Number(this.props.productDetails.price)),

        }, () => {
            if ((!this.state.titleError && !this.state.descriptionError && !this.state.priceError)) {
                this.popUp();
            }
        })

    }

    render() {
        let title = this.props.productDetails.title;
        let description = this.props.productDetails.description;
        let price = this.props.productDetails.price;
        return (
            <div className='update'>

                {
                    this.state.popUp &&
                    <div className='popup-container'>
                        <div className='popUp'>
                            <div className='update-message'>
                                Are you really want to update?
                            </div>
                            <div className='button-container'>
                                <button className='popup-button' onClick={this.confirm}>Yes</button>
                                <button className='popup-button' onClick={this.cancel}>No</button>
                            </div>


                        </div>
                    </div>

                }

                {!this.props.productSelectFlag &&
                    !this.props.loaderFlag &&
                    <div className='update-section'>
                        <h1>Update section</h1>
                        <h3>Select the product for edit details</h3>
                        <div></div>
                    </div>
                }

                {this.props.updatedFlag &&
                    <div className='update-section'>
                        <h1>Update Successfully</h1>
                        <h3>Select another product for edit details</h3>
                        <div></div>

                    </div>
                }

                {
                    this.props.productSelectFlag &&
                    !this.props.updatedFlag &&
                    this.state.updateField &&
                    <div className='container'>
                        <h1>Update Product details</h1>
                        <form className="form-container" >
                            <div className="label">Title</div>
                            <input id="title" type="text" placeholder="Update product title" name="title" onChange={this.props.handleChange} value={title} />
                            {
                                this.state.titleError &&
                                <div className='errorLable'>Title is invalid.</div>
                            }

                            <div className="label">Description</div>
                            <textarea id="description" type="textarea" placeholder="Update product description" name="description" value={description} onChange={this.props.handleChange} />
                            {
                                this.state.descriptionError &&
                                <div className='errorLable'>Description is invalid.</div>
                            }
                            <br />
                            <br />

                            <div className="lable">Price</div>
                            <input id="price" type="number" placeholder="Update product price" name='price' value={price} onChange={this.props.handleChange} />
                            {
                                this.state.priceError &&
                                <div className='errorLable'>Invailid price</div>
                            }


                            <div className='update-button'><button id="update-button" onClick={this.validation}>Update</button></div>

                        </form>
                    </div>
                }

            </div>
        );
    }
}

export default Update;