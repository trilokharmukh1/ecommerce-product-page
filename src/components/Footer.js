import React from "react";
import './../style/Footer.css'
import logo from './../images/logo.png';

class Footer extends React.Component {
    render() {
        return (

            <footer>
                <img src={logo} alt="footer-logo" />

                <div className="footer-right">
                    <div className="copy-right">
                        © trilok. All Rights Reserved
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;