import React from "react";
import { Link } from "react-router-dom";

class ProductList extends React.Component {

    render() {

        return (
            <div ><p className="all-products-heading">All Products </p> <div className="all-products-container">
                {
                    this.props.products.map((product, index) => {
                        return <div className="product-container" key={product.id} >
                            <img src={product.image} alt="product" /><br />
                            <div className="product-details">
                                <h3 className="product-title">{product.title}</h3> <br />
                                <p className="product-description">{product.description}</p> <br />
                                <p className="product-price">Price: <span>${product.price}</span></p>
                                <div className="product-rating">
                                    Rating: {product.rating.rate} ({product.rating.count})
                                </div><br />
                                <div className="button-container">
                                    <button className="product-update-button product-button"
                                        onClick={() => this.props.getProductData(product, index, false)}>
                                        Update product
                                    </button><br />

                                    <Link to={{
                                        pathname: `/viewproduct:${product.id}`
                                    }} >
                                        <button className="view-button product-button">
                                            View product
                                        </button>
                                    </Link><br />

                                    <button className="addToCart-button product-button"
                                        onClick={() => this.props.addToCart(product)}>
                                        Add to cart
                                    </button><br />

                                </div>
                            </div>
                        </div>
                    })
                }
            </div></div>
        );
    }
}

export default ProductList;