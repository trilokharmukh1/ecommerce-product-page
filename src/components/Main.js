import React from "react";
import axios from 'axios'
import Product from "./Product";
import Update from "./Update";

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allProducts: "",
            loaderFlag: true,
            productDetails: "",
            errorFlag: false,
            productSelectFlag: false,
            updatedFlag: "",
            updateProduct: "",
            index: "",
            noProductFlag: false,
        }
    }

    componentDidMount() {
        axios({
            url: "products",
            baseURL: "https://fakestoreapi.com",
            method: "get",
        })
            .then(response => {
                this.setState({ allProducts: response.data },
                    (()=>{
                        if(response.data.length===0){
                            this.setNoProductFlag();
                        }
                    }));
            }).catch(error => {
                this.setErrorFlag();
                console.error('There was an error!', error);
            }).finally(() => {
                this.setState({ loaderFlag: false })
            })
    }

    getProductData = (data, index, flag) => {
        this.setState({
            productDetails: data,
            index: index,
            productSelectFlag: true,
            updatedFlag: flag
        })
    }

    setUpdatedFlag = () => {
        this.setState({ updatedFlag: true })
    }

    setErrorFlag = () => {
        this.setState({ errorFlag: true })
    }

    setNoProductFlag = () =>{
        this.setState({ noProductFlag: true })
    }
    handleChange = (event) => {
        let name = event.target.name
        this.setState((state) => ({
            productDetails: {
                ...state.productDetails,
                [name]: event.target.value.trim(),
            }
        }),
        )
    }

    updateDetails = () => {
        if (this.state.productDetails.title === "") {

        }

        this.setState({
            allProducts: this.state.allProducts.map((product, index) => {
                if (index === this.state.index) {
                    product = this.state.productDetails;
                }
                return product
            })
        });
        this.setUpdatedFlag();

    }

    render() {
        return (
            <div className="main-container">

                <Product
                    allProducts={this.state.allProducts}
                    loaderFlag={this.state.loaderFlag}
                    getProductData={this.getProductData}
                    errorFlag={this.state.errorFlag}
                    addToCart={this.props.addToCart}
                />
                {
                    !this.state.errorFlag &&
                    !this.state.noProductFlag &&
                    <Update
                        loaderFlag={this.state.loaderFlag}
                        productSelectFlag={this.state.productSelectFlag}
                        updatedFlag={this.state.updatedFlag}
                        handleChange={this.handleChange}
                        updateDetails={this.updateDetails}
                        productDetails={this.state.productDetails}
                    />
                }
            </div>
        );
    }
}

export default Main;