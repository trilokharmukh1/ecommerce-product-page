import './../style/Cart.css'
import React from 'react';
import emptyCart from './../images/emptyCart.png'

class Cart extends React.Component {
    render() {
        return (
            <div className="cart">


                {
                    this.props.onCart.length === 0 &&
                    <div className='empty-cart'>
                        <img className='empty-cart-image' src={emptyCart} alt="empty-cart" />
                        <div className='empty-message'>Your cart is empty</div>
                    </div>
                }

                <div className='all-cart-container'>
                    {this.props.onCart.length > 0 &&
                        this.props.onCart.map((item) => {
                            return <div className="cart-container" key={item.id} >
                                <img src={item.image} alt="product" /><br />
                                <div className="product-details">
                                    <h3 className="product-title">{item.title}</h3> <br />
                                    {/* <p className="product-description">{item.description}</p> <br /> */}
                                    
                                    <div className='price-rating'>
                                        <p className="product-price">Price: <span>${item.price}</span></p>
                                        <div className="product-rating">
                                            Rating: {item.rating.rate} ({item.rating.count})
                                        </div>

                                    </div>
                                    <div className='total-price'> Total Price : <span> ${((item.price) * (item.quantity)).toFixed(2)} </span> </div><br />

                                    <div className="cart-button-container">
                                        <button className="increment-button cart-button"
                                            onClick={() => this.props.addToCart(item)}> +
                                        </button><br />

                                        <div className='item-quanitity'>
                                            {item.quantity}
                                        </div>

                                        <button className="decrement-button cart-button"
                                            onClick={() => this.props.decreaseQuantity(item)}> -
                                        </button><br />

                                        <button className="remove-button cart-button"
                                            onClick={() => this.props.removeFromCart(item)}>
                                            Remove
                                        </button><br />
                                    </div>
                                </div>
                            </div>
                        })
                    }
                </div>

                {
                    this.props.onCart.length !== 0 &&
                    <div className='final-cart'>
                        <div className='cart-heading'>
                            <h3>Cart Summary</h3>
                            <hr />
                            <div className='total-items total-summary' >
                                <div>Total Items</div>
                                <div>{this.props.onCart.length}</div>
                            </div>

                            <div className='total-price total-summary'>
                                <div>Total Price</div>
                                <div>
                                    $   {this.props.onCart.reduce((totalPrice, item) => {
                                        return totalPrice + (((item.price) * (item.quantity)))
                                    }, 0).toFixed(2)}
                                </div>
                            </div>

                        </div>

                    </div>
                }

            </div>
        )
    }
}

export default Cart;