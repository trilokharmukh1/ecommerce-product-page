import React from "react";
import Loader from "./Loader";
import './../style/Product.css';
import ProductList from "./ProductList";

class Product extends React.Component {
    render() {
     
        return (
            <article id="article-one">
                {
                    this.props.errorFlag &&
                    this.props.allProducts === "" &&
                    !this.props.loaderFlag &&
                    <div className="message">Error occurred while fetching the products.</div>
                }

                {
                    this.props.allProducts.length === 0 &&
                    !this.props.errorFlag &&
                    this.props.allProducts !== "" &&    
                    <div className="message">No product available</div>
                }

                {
                    this.props.allProducts.length > 0 &&
                    !this.props.errorFlag &&
                    !this.props.loaderFlag &&
                    <ProductList
                        products={this.props.allProducts}
                        getProductData={this.props.getProductData}
                        viewProduct={this.props.viewProduct}
                        loaderFlag={this.props.loaderFlag}
                        addToCart={this.props.addToCart}
                    />
                }

                {
                    this.props.loaderFlag &&
                    !this.props.errorFlag &&
                    this.props.allProducts === "" &&
                    <Loader/>
                }
            </article>
        );
    }
}

export default Product;