import React from 'react';
import './../style/Header.css';
import { Link } from 'react-router-dom';
import logo from './../images/logo.png';
import cart from './../images/cart.png';

class Header extends React.Component {
    render() {
        return (
            <header>
                <img src={logo} className="header-logo" alt="logo" />
                <nav>
                    <ul>
                        <Link to="/">
                            <li>Home</li>
                        </Link>
                    </ul>
                </nav>

                <div className="header-cart">
                    <Link to="/cart">
                        <img src={cart} alt="cart" />
                        <span>{this.props.onCart.length}</span>
                    </Link>
                </div>
            </header>
        );
    }
}

export default Header;