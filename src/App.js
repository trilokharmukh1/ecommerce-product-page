import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Header from './components/Header';
import Main from './components/Main';
import Footer from './components/Footer';
import ViewProduct from './components/ViewProduct';
import Cart from './components/Cart';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loaderFlag: true,
      cart: []
    }
  }

  addToCart = (product) => {
    console.log("addTocart");

    let productAvailable = this.state.cart.find((productInCart) => {
      return productInCart.id === product.id;
    })

    if (productAvailable) {
      this.setState({
        cart: this.state.cart.map((productInCart) => {
          if (productInCart.id === product.id) {
            return { ...productInCart, quantity: productInCart.quantity + 1 }
          } else {
            return productInCart
          }
        })
      })

    } else {
      this.setState({
        cart:
          [...this.state.cart, { ...product, quantity: 1 }]

      },
        () => {
          console.log(this.state.cart);
        })

    }

  }


  decreaseQuantity =(product)=>{
    let productAvailable = this.state.cart.find((productInCart) => {
      return productInCart.id === product.id;
    })

    if (productAvailable.quantity===1) {
      this.setState({
        cart: this.state.cart.filter((product)=>{
          return product.id !== productAvailable.id
        })
      })

    } else {
      this.setState({
        cart: this.state.cart.map((productInCart) => {
          if (productInCart.id === product.id) {
            return { ...productInCart, quantity: productInCart.quantity - 1 }
          } else {
            return productInCart
          }
        })
      })
    }
  }

  removeFromCart = (product) =>{
    this.setState({
      cart: this.state.cart.filter((item)=>{
        return item.id !== product.id
      })
    })
  }


  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Header onCart={this.state.cart}/>
          <Switch>

            <Route
              path="/" exact
              render={(props) => (
                <Main {...props}
                  addToCart={this.addToCart}
                />
              )}
            />


            <Route
              path="/viewproduct:id"
              render={(props) => (
                <ViewProduct {...props}
                  addToCart={this.addToCart}
                />
              )}
            />
            
            <Route path="/cart"
              render={(props)=>(
                <Cart {...props}
                onCart={this.state.cart}
                addToCart={this.addToCart}
                decreaseQuantity={this.decreaseQuantity}
                removeFromCart={this.removeFromCart}
                />
              )}
            />
          </Switch>
          <Footer />
        </BrowserRouter>

      </div>
    );
  }
}

export default App;
